var express = require('express');
var router = express.Router();
var audioController =  require('../Controllers/AudioController');

router.get("/getUrl",audioController.uploadFile);

module.exports = router;